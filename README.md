# conductor_playground

A playground for experimenting, developing and testing Netflix OSS Conductor based workflows.

# Useful information

## launch playground
```docker-compose -f stack-playground.yaml up```

## Upload workflow with:
Put mioworkflow.json into workflows dir, then:

```./upload-workflow.sh mioworkflow```

## Delete workflow with:
If 1 is the version to remove and mioworkflow is the name given to the workflow

```./delete-workflow.sh mioworkflow/1```

## Useful links

Conductor REST API:

```http://localhost:8080/api```

Access UI:

```http://localhost:5000/```

Official Site:

```https://conductor.netflix.com/```

## Task documentation

pyrest task inputParameters are configured like this:

```
{
    "method" : "POST",
    "url" : "http://...."
    "headers" : {
        "Authorization" : "...",
        "Content-Type" : "....",
        "Accept" : "...",
        ...
    },
    "params" : { // Query string parameters

    },
    "body" : {  //either a map of form-parameters or a JSON structure or a string body

    },
    "expect" : [200,409], // Default is 20x
    "fail" : true|false   #if failure should be avoided at all. If set to true the task succeeds independently of the HTTP Status
}
```

pyrest output is as follows:

```
{
    "body" : //the response body possibly parsed to JSON if Accept is set to "application/json" or response contains a mime-type indication of "applciation/json"
    "headers" : {
        // a map containing the response headers
    },
    "status" : 200,
    "readon" : "OK"
}
```

## Run java worker example

Run 

```
./upload-workflow.sh test_myworker_workflow.json
```

Increase the number stored by the init task in the variable cycles in order cause an exception (values > 7). 

To start the my_worker implementing my_task enter directory jworker and run:

```
mvn clean compile exec:java
```

Access the UI and from the workbench run a test_myworker workflow. You should be able to see log information during execution of the task updating at every second.
