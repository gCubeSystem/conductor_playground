package com.nubisware;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.UUID;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;

import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MyWorker {
 
    protected static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    protected static final String taskdefUrl = "/api/metadata/taskdefs";
    protected static final String taskPollUrl = "/api/tasks/poll";
    protected static final String taskUpdateUrl = "/api/tasks";

    final protected String url;
    final protected String taskName;
    final protected UUID workerId;

    protected OkHttpClient client;

    public MyWorker(String url, String taskName){
        this.url = url;
        this.taskName = taskName;
        this.workerId = UUID.randomUUID();

        this.client = new OkHttpClient();

        this.uploadTaskDefinition();
    }

    protected void uploadTaskDefinition(){
        JsonObject tdef = Json.createObjectBuilder()
            .add("name", this.taskName)
            .add("ownerEmail", "m.lettere@gmail.com")
            .add("description", "Example task doing nothing actually")
            .build();
        JsonArray tdefs = Json.createArrayBuilder()
            .add(tdef)
            .build();
        HttpUrl u = HttpUrl.parse(this.url + taskdefUrl);
        RequestBody body = RequestBody.create(tdefs.toString(), JSON);
        Request req = new Request.Builder()
            .url(u)
            .post(body)
            .build();
        try {
            Response resp = this.client.newCall(req).execute();
            System.out.println(resp.code());
            System.out.println(resp.body().string());
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    protected JsonObject pollTask(){
        JsonObject task = null;
        HttpUrl u = HttpUrl.parse(this.url + taskPollUrl + "/" + this.taskName + "?workerid=" + this.workerId);
        Request req = new Request.Builder().url(u).get().build();
        System.out.println("Polling ... "); 
        while(task == null){
            try {
                //System.out.println("Polling ... ");
                Response resp = this.client.newCall(req).execute();
                //System.out.println(resp.code());
                if(resp.code() == 200){
                    task = Json.createReader(new StringReader(resp.body().string())).readObject();
                    System.out.println("Task is " + task.getJsonObject("taskDefinition").getString("name"));
                }else if(resp.code() != 204){
                    System.err.println("Error while polling ... " + resp.body().string());
                }
                Thread.sleep(1000);
            } catch (Exception e) {
                System.err.println(e);
            }
        }
        return task;
    }

    protected void updateTask(JsonObject task, String status, ArrayList<String> logs){
        JsonArrayBuilder logsBuilder = Json.createArrayBuilder();
        for(String l: logs){
            logsBuilder.add(l);
        }
        JsonObject tstat = Json.createObjectBuilder()
            .add("status", status)
            .add("logs", logsBuilder.build())
            .add("description", "Example task doing nothing actually")
            .add("taskId", task.getString("taskId"))
            .add("workflowInstanceId", task.getString("workflowInstanceId"))
            .build();
        
        HttpUrl u = HttpUrl.parse(this.url + taskUpdateUrl);
        RequestBody body = RequestBody.create(tstat.toString(), JSON);
        Request req = new Request.Builder()
            .url(u)
            .post(body)
            .build();
        try {
            Response resp = this.client.newCall(req).execute();
            System.out.println(resp.code());
            System.out.println(resp.body().string());
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    protected void handleTask(JsonObject task){
        String name = task.getJsonObject("taskDefinition").getString("name");
        String id = task.getString("taskId");
        System.out.println("Handling task " + id + " " + name);
        int cycles = task.getJsonObject("inputData").containsKey("cycles") ? 
            task.getJsonObject("inputData").getInt("cycles") : 1;
        ArrayList<String> logs = new ArrayList<String>();
        for(int i=0; i < cycles; i++){
            try {
                logs.add("Entering cycle " + i);
                Thread.sleep(1000);
                logs.add("After cycle " + i + " still ok");
                this.updateTask(task, "IN_PROGRESS", logs);
                logs.clear();
                if(i % 7 == 6) throw new Exception("Force fail");
            } catch (Exception e) {
                e.printStackTrace();
                logs.add("At cycle " + i + " I've been interrupted");
                this.updateTask(task, "FAILED", logs);
            }
            
        }
        this.updateTask(task, "COMPLETED", logs);
    }

    protected void run(){
        while(true){
            JsonObject t = this.pollTask();
            this.handleTask(t);
        }
    }

    public static void main( String[] args ){
        System.out.println( "Starting MyWorker!" );
        MyWorker w = new MyWorker("http://localhost:8080", "mytask");
        w.run();
    }
}
